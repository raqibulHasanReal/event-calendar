import React, { Component } from 'react'
import Calendar from './Components/Calendar/index.js'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import AddEvent from './Components/AddEvent/index.js'
import Event from './Components/Events/index.js'
import EventsShow from './Components/EventsShow/index.js'
import EditEvent from './Components/EditEvent/index.js'
import moment from 'moment';
import _ from 'lodash'
import Navbar from './Components/Navbar/index'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addEvent: false,
      eventField:false,
      eventEditField:false,
      date:'',
      eventName:'',
      eventDescription:'',
      events: [],
      willEditEvent:null,
      dateContext: moment(),
      editedEventIndex:'',
    };
  }

  componentWillMount() {
    localStorage.getItem('events') && this.setState({
      events:JSON.parse(localStorage.getItem('events')),
    })  
  }

  componentWillUpdate(nextProps, nextState) {
    localStorage.setItem('events', JSON.stringify(nextState.events));
    localStorage.setItem('eventsData', Date.now());
  }

  month = () => {
    return this.state.dateContext.format('MMMM');
  }

  year = () => {
    return this.state.dateContext.format('Y');
  }

  onDayClick = (e, date) => {
    this.setState({
      ...this.state,
      addEvent:true,
      eventField:true,
      eventEditField: false,
      date,
      eventName:'',
      eventDescription:'',
    });
  }

  onCloseClick = ()=> {
    this.setState({
      ...this.state,
      addEvent: false,
      eventField: false,
      eventEditField: false,

    })
  }

  onNameChange = (event) => {
    this.setState({
      ...this.state,
      eventName: event.target.value,
    });   
  }

  onDescChange = (event) => {
      event.preventDefault()
    this.setState({
      ...this.state,
      eventDescription: event.target.value
    }); 
  }

  onAddEventClick() {
    if(this.state.eventName === '') {
      document.getElementById('event-name').focus();
    
    } else if(this.state.eventDescription === '') {
     
      document.getElementById('event-desc').focus();
    } else{
      let date = this.state.date;
      let id = Math.random()
      let month = this.month();
      let year = this.year();
      let name = this.state.eventName;
      let disc = this.state.eventDescription;
      let events = this.state.events.concat({'id':id, 'date':date, 'month':month,'year':year, 'name':name, 'disc':disc});
      
      this.setState({
        ...this.state,
        events,
        eventName:'',    
        eventDescription:''
      });
    }
  }

  deleteHandler = (eventObj) => {

    if (window.confirm("Do you really want to delete this !!")) { 
      
      let eventIndex = this.state.events.findIndex(event => _.isEqual(event, eventObj));
      let events = this.state.events;
      if (eventIndex > -1) {
        events.splice(eventIndex, 1);
      }
  
      this.setState({
        ...this.state,
        events,
        eventDescription:'',
        eventName: '',
        editedEventDesc: '',
        editedEventName: '',
        eventEditField:false,
        addEvent:true,
      })
    }
  }

  editHandler = (dayEvent) => {
    let willEditEvent = _.cloneDeep(dayEvent);

    this.setState({
      ...this.state,
      addEvent:false,
      eventEditField:true,
      willEditEvent,
    })
  }

  onNameEdit = (event) => {
    let willEditEvent = this.state.willEditEvent;
    willEditEvent.name = event.target.value;
    
    this.setState({
      ...this.state,
      willEditEvent
    }); 
  }
  
  onDescEdit = (event) => {
    let willEditEvent = this.state.willEditEvent;
    willEditEvent.disc = event.target.value;

    this.setState({
      ...this.state,
      willEditEvent
    }); 
  }

  onEditEventClick = () => {
    let willEditEvent = this.state.willEditEvent;
    let events = this.state.events;

    if (!willEditEvent.name) {
      alert('Event name cannot be empty')
      document.getElementById('event-name').focus();
      return
    } else if (!willEditEvent.disc) {
      alert('Event description cannot be empty');
      document.getElementById('event-desc').focus();
      return
    }

    let event = events.find(event=> {
      return event.id ===willEditEvent.id
    })

    event.name = willEditEvent.name;
    event.disc = willEditEvent.disc;

    this.setState({
      events,
      willEditEvent: null,
      editedEventIndex: null,
      eventEditField: false,
      addEvent:true
    });;
  }

  render() {
    return (
      <div>
        <div>
          <Navbar/>
        </div>

        <div>
          <div className="col-sm-10 offset-sm-1 mt-4 App">
            <a href="/#" id="month-name">{this.month()}  {this.year()}</a>

            <div className="row mt-3">
              <div className="col-sm-8">
                <Calendar
                  onDayClick = {(e, date) => this.onDayClick(e, date)}
                  events = {this.state.events}
                />

                <EventsShow
                  events = {this.state.events}
                  appState={ this.state }
                />
              </div>
              <div className="col-sm-4">
                <AddEvent 
                  appState = { this.state }
                  onNameChange = {(event) => this.onNameChange(event)}
                  onDescChange = {(event) => this.onDescChange(event)}
                  onAddEventClick = {() => this.onAddEventClick()}
                  onCloseClick = {() => this.onCloseClick()}
                  />

                <EditEvent
                  appState = { this.state }
                  onCloseClick = {() => this.onCloseClick()}
                  onNameEdit = {(event) => this.onNameEdit(event)}
                  onDescEdit = {(event) => this.onDescEdit(event)}
                  onEditEventClick = {() => this.onEditEventClick()}
                />
                
                <Event
                  events={this.state.events}
                  appState = {this.state}
                  editHandler = { (dayEvent) => this.editHandler(dayEvent)}
                  deleteHandler = { (dayEvent) => this.deleteHandler(dayEvent)}
                />
              </div>
            </div>

          </div>
        </div>
      </div>   
    );
  }
}

export default App;
