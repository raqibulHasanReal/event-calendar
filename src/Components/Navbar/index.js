import React from 'react';

const Navbar = (props) => {
    return(
        <div>
            <nav className="navbar navbar-expand-sm bg-info navbar-dark">
                <a className="navbar-brand logo-color" href="/#">EVENT CALENDAR</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link" href="https://www.youtube.com/watch?v=vhajHB51UCc&feature=youtu.be">Demo</a>
                        </li>
                    </ul>
                </div>  
            </nav>
        </div>
    )
}

export default Navbar;