import React, { Component } from 'react';
import moment from 'moment';
import './calendar.css';

class Calendar extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            dateContext: moment(),
         };
    }
    
    weekdaysShort = moment.weekdaysShort();

    daysInMonth = () => {
        return this.state.dateContext.daysInMonth();
    }

    currentDay = () => {
        return this.state.dateContext.format("D");
    }

    firstDayOfMonth = () => {
        let dateContext = this.state.dateContext;
        let firstDay = moment(dateContext).startOf('month').format('d');
        return firstDay;
    }

    onDayClick = (e, day) => {
        this.props.onDayClick && this.props.onDayClick(e, day);
    } 

    render() {
        let weekdays = this.weekdaysShort.map((day) => {
            return (
                <td key={day} className="weed-day">{day}</td>
            )
        });

        let blanks = [];
        for(let i = 0; i<this.firstDayOfMonth(); i++) {
            blanks.push(
                <td key={i*80} className="emptySlot">
                    {""}
                </td>
            );
        }

        let daysInMonth = [];
        for(let date = 1; date <= this.daysInMonth(); date++) {
            let className = (date === parseInt(this.currentDay())  ? "day current-day" : "day");
            className += (this.props.events.map(event => event.date).includes(date)) ? ' has-event' : '';
    
            daysInMonth.push(
                <td tabIndex={date} events={this.props.events} onClick={(e)=> this.onDayClick(e, date)} key ={date} className={className}>
                    <span>{date}</span>
                </td>
            );
        }

        var totalSlots = [...blanks, ...daysInMonth];
        let dates = [];
        let cells = [];
        totalSlots.forEach((row, i) => {

            if((i % 7)!== 0) {
                cells.push(row);
            } else {
                let insertRow = cells.slice();
                dates.push(insertRow);
                cells = [];
                cells.push(row);
            }

            if( i === totalSlots.length - 1) {
                let insertRow = cells.slice();
                dates.push(insertRow);
            }
        });

        let trElems =[];
        trElems = dates.map((date, i) => {
            return(
                <tr key={i*100}>
                    {date}
                </tr>
            )
        });

        return (
            <div className="calendar-container" >
                <table className="calendar">
                    <tbody className="">
                        <tr className="weeks text-center">
                            { weekdays }
                        </tr>
                        {trElems}
                    </tbody>   
                </table>
            </div>   
        );
    }
}

export default Calendar;