import React from 'react';

const Event = (props)=>  {

    let onDayEventField = null;

    let events = props.events.map((event) => {
        return event
      });  
      
    let onDayEvent = events.filter((event)=> {
        return event.date === props.appState.date
      });

    let today = new Date().getDate();

    if(props.appState.eventField) {

        let className = ( today <= props.appState.date  ? "form-group p-3 mt-1 future-event-color" : "form-group p-3 mt-1 old-event-color");

        onDayEventField = onDayEvent.map((dayEvent, index) => { 
            return (
                <div className="mt-3" key={ index }>
                    <div className={ className }>
                        <div className="d-flex">
                            <div>                 
                                <h4 > {dayEvent.name}</h4>
                                <small className="form-text">{dayEvent.disc}</small> 
                            </div>
                            <div className="ml-auto edit-icon">
                                <span><i onClick={ () => props.editHandler(dayEvent) } className="fas fa-edit mr-3"></i></span>     
                                <span><i onClick={ ()=> props.deleteHandler(dayEvent)} className="fas fa-trash"></i></span>       
                            </div>
                        </div>
                    </div>
                </div>
            );
        });
    }

    return (
        <div>          
            {onDayEventField}
        </div>
    );
  }

export default Event;





