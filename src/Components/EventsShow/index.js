import React from 'react'
import './eventShow.css'

const EventsShow = (props) => {

    let events = props.events.map((event) => {
        return event
      });

    let allEvents = null;

    var today = new Date().getDate();

    allEvents = events.map((event) => { 
        let className = ( today <= event.date  ? "form-group p-3 mt-1 future-event-color" : "form-group p-3 mt-1 old-event-color");
        
        return (
            <div className="mt-3" key={Math.random()}>
                <div className={ className }>
                    <div className="d-flex">
                        <div className="mr-5 ml-5">
                            <h3>{event.date} {event.month} {event.year}</h3>
                        </div>
                        <div className="m-auto">                 
                            <h4 > {event.name}</h4>
                            <small className="form-text">{event.disc}</small> 
                        </div>
                    </div>
                </div>
            </div>
        );
    });

    return(
        <div className="events-containe mt-1 p-1">
            {allEvents}
        </div>
    )
}

export default EventsShow;