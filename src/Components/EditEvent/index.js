import React from 'react'

const EditEvent = (props) => {

    let editEventField = null;
    let month = props.appState.dateContext.format('MMMM');
    let year = props.appState.dateContext.format('Y');

    if(props.appState.eventEditField) {
        editEventField = (
        <div>            
            <div className="d-flex">
            <h3> {props.appState.date} {month}, {year}</h3>
                <button onClick={()=> props.onCloseClick()} type="button" className="btn btn-info ml-auto">Cancle</button>
            </div>
            
            <div className="form-group">
                <label htmlFor="event-name">event Name <span>*</span></label>
                <input onChange={props.onNameEdit} type="text" className="form-control" id="event-name" value={props.appState.willEditEvent.name} placeholder="Event Name" required/>
            </div>

            <div className="form-group">
                <label htmlFor="event-desc">Description <span>*</span></label>
                <input type="text" className="form-control" id="event-desc" placeholder="Description" value={props.appState.willEditEvent.disc} onChange={props.onDescEdit}  required/>
            </div>
            
            <button onClick={()=> props.onEditEventClick()} type="button" className="btn btn-primary btn-block">Update</button>                        
        </div>
      ) 
    }


    return(
        <div>
            { editEventField }
        </div>
    )
}

export default EditEvent;