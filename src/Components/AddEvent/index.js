import React from 'react';

const AddEvent = (props) => {

    let { month, year } = getCalendarObjects(props);

    let addEventField = null;

    if(props.appState.addEvent) {
      addEventField = (
        <div>            
            <div className="d-flex">
                <h3> {props.appState.date} {month}, {year}</h3>
                <button onClick={()=> props.onCloseClick()} type="button" className="btn btn-info ml-auto">Close</button>
            </div>
            
            <div className="form-group">
                <label htmlFor="event-name">event Name <span>*</span></label>
                <input onChange={props.onNameChange} type="text" className="form-control" id="event-name" value={props.appState.eventName} placeholder="Event Name" required/>
            </div>

            <div className="form-group">
                <label htmlFor="event-desc">Description <span>*</span></label>
                <input type="text" className="form-control" id="event-desc" placeholder="Description" value={props.appState.eventDescription} onChange={props.onDescChange}  required/>
            </div>
            
            <button onClick={()=> props.onAddEventClick()} type="button" className="btn btn-primary btn-block">Add Event</button>                        
        </div>
      ) 
    }
  
    return (
        <div>          
            { addEventField }
        </div>
    );
}

export default AddEvent;

function getCalendarObjects(props) {
    let month = props.appState.dateContext.format('MMMM');
    let year = props.appState.dateContext.format('Y');
    return { month, year };
}

